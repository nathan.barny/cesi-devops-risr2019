# CESI DevOps - TP

* [IaC Intro](./iac-intro)

## Environnement recommandé

Avant de commencer, équipez vous de :
* un hyperviseur
  * [VirtualBox](https://www.virtualbox.org/) est **fortement** recommandé (compatibilité forte avec Vagrant)
  * Hyper-V est aussi possible
  * je déconseille VMWare Workstation (ou autres produits VMWare)
* [Vagrant](https://www.vagrantup.com/)
* Un IDE
  * `vim` bien sûr !
  * [VSCode](https://code.visualstudio.com/)
* un client et une paire de clés SSH
* un client pour utiliser Git
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
